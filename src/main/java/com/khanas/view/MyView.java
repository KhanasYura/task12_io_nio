package com.khanas.view;

import com.khanas.Model.Buffer;
import com.khanas.Model.Dog;
import com.khanas.Model.PathGetter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Logger logger = LogManager.getLogger();
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethod;

    public MyView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1.Task1");
        menu.put("2", "2.Task2");
        menu.put("3", "3.Task3");
        menu.put("4", "4.Task4");
        menu.put("5", "5.Task5");
        menu.put("6", "6.Task6");
        menu.put("Q", "Q.Exit");

        menuMethod = new LinkedHashMap<>();
        menuMethod.put("1", this::task1);
        menuMethod.put("2", this::task2);
        menuMethod.put("3", this::task3);
        menuMethod.put("4", this::task4);
        menuMethod.put("5", this::task5);
        menuMethod.put("6", this::task6);
        show();
    }

    private void task1() {
        Dog dog = new Dog("Barbos", "Beagle", "Small", 4);
        logger.info(dog.toString());

        try (ObjectOutputStream out =
                     new ObjectOutputStream(
                             new FileOutputStream("Dog.txt"))) {
            out.writeObject(dog);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream in =
                     new ObjectInputStream(
                             new FileInputStream("Dog.txt"))) {
            Dog myNewDog = (Dog) in.readObject();
            logger.info(myNewDog.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void task2() {

        logger.info("Usual Reader");

        try {
            InputStream in = new FileInputStream("info.pdf");
            long start = System.currentTimeMillis();
            int data = in.read();
            int count = 0;

            while (data != -1) {
                data = in.read();
                count++;
            }
            in.close();

            logger.info(count);

            long timeWorkCode = System.currentTimeMillis() - start;
            logger.info("Took time : " + timeWorkCode);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("Buffered Reader");

        int bufferedSize = 1 * 1024;

        try {
            DataInputStream in = new DataInputStream(
                    new BufferedInputStream(
                            new FileInputStream(
                                    "info.pdf"), bufferedSize));
            long start = System.currentTimeMillis();
            byte data = in.readByte();
            int count = 0;

            while (data != -1) {
                data = in.readByte();
                count++;
            }
            in.close();

            logger.info(count);

            long timeWorkCode = System.currentTimeMillis() - start;
            logger.info("Took time : " + timeWorkCode);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void task3() {
        PrintWriter pw = new PrintWriter(System.out, true);
        String str = "Lorem ipsum dolor sit amet, consectetur"
                + " adipiscing elit, sed do eiusmod tempor incididunt "
                + "ut labore et dolore magna aliqua. ";
        ByteArrayInputStream in = new ByteArrayInputStream(str.getBytes());
        PushbackInputStream pin = new PushbackInputStream(in);
        try {
            int c = pin.read();
            while (c != -1) {
                pw.print((char) c);
                c = pin.read();
            }
            pin.read(str.getBytes(), 0, str.length());
            for (int i = 0; i < str.length(); i++) {
                pw.print(str.charAt(i));
            }
            pw.print("\n");
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void task4() {
        try {
            logger.info("Enter name of file: ");
            String fileName = sc.nextLine();
            BufferedReader in = new BufferedReader(new FileReader(fileName));

            String data = in.readLine();

            while (data != null) {
                data = data.trim();
                if (data.length() > 2) {
                    if (data.substring(0, 2).equals("//")
                            || data.substring(0).equals("/**")
                            || data.substring(0, 2).equals("/*")
                            || data.substring(0, 1).equals("*")
                            || data.substring(data.length() - 2).equals("*/")) {
                        logger.info(data);
                    }
                }
                data = in.readLine();
            }
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void task5() {
        PathGetter.getPath();
    }

    private void task6() {
        logger.info("-----------Read from Channel-----------");
        Buffer.readfromFile();
        logger.info("----------- Write to Channel-----------");
        Buffer.writetoFile();
    }

    private void outputMenu() {
        logger.info("MENU:");

        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    private void show() {
        String keyMenu;

        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();

            if (keyMenu.equals("Q")) {
                break;
            }

            menuMethod.get(keyMenu).print();
        } while (true);
    }


}
