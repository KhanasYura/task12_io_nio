package com.khanas.Model;

import java.io.Serializable;

public class Animal implements Serializable {

    private String name;

    public Animal() {
    }

    public Animal(final String name) {
        this.name = name;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }
}
