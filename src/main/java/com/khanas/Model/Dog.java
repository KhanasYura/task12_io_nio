package com.khanas.Model;

public class Dog extends Animal {

    private String breed;
    private String size;
    private transient int age;

    public Dog(final String name, final String breed,
               final String size, final int age) {
        this.setName(name);
        this.breed = breed;
        this.size = size;
        this.age = age;
    }

    public final String getBreed() {
        return breed;
    }

    public final void setBreed(final String breed) {
        this.breed = breed;
    }

    public final String getSize() {
        return size;
    }

    public final void setSize(final String size) {
        this.size = size;
    }

    public final int getAge() {
        return age;
    }

    public final void setAge(final int age) {
        this.age = age;
    }

    @Override
    public final String toString() {
        return "Dog{" + "Name= " + this.getName()
                + "breed='" + breed + '\''
                + ", size='" + size + '\''
                + ", age=" + age + '}';
    }
}
