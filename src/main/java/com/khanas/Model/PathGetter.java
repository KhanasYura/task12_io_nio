package com.khanas.Model;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Scanner;

public class PathGetter {
    private static Scanner sc = new Scanner(System.in);

    public static void getPath() {
        Path p = Paths.get(System.getProperty("user.home"));
        while (true) {
            System.out.println(p);
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(p)) {
                int i = 1;
                ArrayList<String> foldersList = new ArrayList<>();
                for (Path file : stream) {
                    String check = file.getFileName().toString();
                    System.out.printf("[%d] %s%n", i++, check);
                    foldersList.add(check);
                }
                System.out.printf("~[%d] return%n", i++);
                System.out.printf("~[%d] Break%n", -1);

                System.out.println("Directory: ");
                int option = sc.nextInt();
                if (option == -1) {
                    break;
                } else if (option < 1 || option > i) {
                    System.err.println("error");
                } else if (option < i - 1) {
                    if (Files.isDirectory(p.resolve(
                            foldersList.get(option - 1)))) {
                        p = p.resolve(foldersList.get(option - 1));
                    } else {
                        System.err.println("it isn't a directory");
                    }
                } else if (option == i - 1) {
                    p = p.getRoot().resolve(p.getParent());
                } else {
                    System.out.println("Wrong input");
                }
            } catch (IOException | DirectoryIteratorException x) {
                System.err.println("error!");
            }
        }
    }
}
