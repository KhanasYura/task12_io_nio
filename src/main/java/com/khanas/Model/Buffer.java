package com.khanas.Model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Scanner;

public class Buffer {

    private static Scanner sc = new Scanner(System.in);

    public static void readfromFile() {

        RandomAccessFile file;
        try {
            file = new RandomAccessFile("Comment.java", "rw");
            FileChannel inChannnel = file.getChannel();
            ByteBuffer buf = ByteBuffer.allocate(256);
            int bytes = inChannnel.read(buf);

            while (bytes != -1) {
                buf.flip();

                while (buf.hasRemaining()) {
                    System.out.print((char) buf.get());
                }

                buf.clear();
                bytes = inChannnel.read(buf);
            }

            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writetoFile() {

        RandomAccessFile file;
        try {
            file = new RandomAccessFile("NewFile.java", "rw");
            FileChannel outChannnel = file.getChannel();

            System.out.println("Text to write to channel");
            byte[] text = sc.nextLine().getBytes();

            ByteBuffer buf = ByteBuffer.wrap(text);
            outChannnel.write(buf);

            outChannnel.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
