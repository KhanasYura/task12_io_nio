package com.khanas.client_server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static Logger logger = LogManager.getLogger();
    private int port;
    private ServerSocket server = null;
    private Socket client = null;
    private ExecutorService pool = null;
    private int clientcount = 0;
    private static ArrayList<BufferedReader> bf = new ArrayList<>();
    private static ArrayList<PrintStream> ps = new ArrayList<>();

    public static void main(final String[] args) throws IOException {
        Server serverobj = new Server(5000);
        serverobj.startServer();
    }

    private Server(final int port) {
        this.port = port;
        pool = Executors.newFixedThreadPool(5);
    }

    private void startServer() throws IOException {

        server = new ServerSocket(5000);
        logger.info("Server Booted");
        while (true) {
            client = server.accept();
            clientcount++;
            ServerThread runnable = new ServerThread(client, clientcount, this);
            pool.execute(runnable);
        }

    }

    private static class ServerThread implements Runnable {

        private Server server;
        private Socket client;
        private BufferedReader cin;
        private PrintStream cout;
        private int id;
        private String s;


        ServerThread(final Socket client, final int count, final Server server)
                throws IOException {

            this.client = client;
            this.server = server;
            this.id = count;
            logger.info("Connection " + id + "established with client "
                    + client);

            cin = new BufferedReader(new InputStreamReader(client
                    .getInputStream()));
            cout = new PrintStream(client.getOutputStream());
            bf.add(cin);
            ps.add(cout);
        }

        public void run() {
            try {
                while (true) {
                    s = cin.readLine();

                    if (s.equalsIgnoreCase("BYE")) {

                        for (PrintStream printStream : ps) {
                            printStream.println("Client(" + id + ") :" + s);
                        }

                        logger.info("Connection ended by client");
                    } else {
                        int index = Character.getNumericValue(s.charAt(0));
                        logger.info("Client(" + id + ") :" + s + "\n");

                        if (index > 0 && index <= ps.size()) {
                            ps.get(index - 1)
                                    .println("Client(" + id + ") :" + s);
                        } else {
                            cout.println("Wrong client");
                        }
                    }
                }

            } catch (IOException ex) {
                logger.error("Error : " + ex);
            }
        }
    }
}
