package com.khanas.client_server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Client {

    private static BufferedReader sin;
    private static PrintStream sout;
    private static BufferedReader stdin;
    private static Logger logger = LogManager.getLogger();

    public static void main(final String[] args) throws Exception {
        Socket sk = new Socket("127.0.0.1", 5000);
        sin = new BufferedReader(new InputStreamReader(sk.getInputStream()));
        sout = new PrintStream(sk.getOutputStream());
        stdin = new BufferedReader(new InputStreamReader(System.in));

        Thread clientWrite = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    String s;
                    try {
                        logger.info("Client : ");
                        s = stdin.readLine();
                        sout.println(s);
                        if (s.equalsIgnoreCase("BYE")) {
                            logger.info("Connection ended by client");
                            break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Thread clientRead = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    String s;
                    try {
                        s = sin.readLine();
                        logger.info("\n" + s + "\n" + "Client :");
                    } catch (IOException e) {
                        break;
                    }
                }
            }
        });

        clientWrite.start();
        clientRead.start();
        clientWrite.join();
        clientRead.interrupt();
        sk.close();
        sin.close();
        sout.close();
        stdin.close();
    }
}
